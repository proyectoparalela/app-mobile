import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { DocumentService, User } from '../../providers';
/**
 * Generated class for the MyFilesPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-my-files',
  templateUrl: 'my-files.html',
})
export class MyFilesPage {
  MyDocuments:any;
  constructor(public navCtrl: NavController, public navParams: NavParams,
    public documento:DocumentService,
    public user: User) {
      this.documento.getByOwner(sessionStorage.getItem("pk")).subscribe((res:any)=>{
      this.MyDocuments=res.data;
      console.log(res);}
      );
    }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MyFilesPage');
  }

}
