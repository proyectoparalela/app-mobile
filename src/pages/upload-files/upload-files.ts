import { Component, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { IonicPage, NavController, ViewController} from 'ionic-angular';
import { CourseService } from '../../providers';
import { TypeService} from '../../providers';
/**
 * Generated class for the UploadFilesPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-upload-files',
  templateUrl: 'upload-files.html',
})
export class UploadFilesPage {
  @ViewChild('fileInput') fileInput;
  isReadyToSave: boolean;
  Items:any;
  item: any;
  courses:any;
  tipos:any;
  form: FormGroup;
    constructor(public navCtrl: NavController, public viewCtrl: ViewController,
       formBuilder: FormBuilder,
       public types: TypeService,
       public course: CourseService) {
      this.course.getCourse().subscribe((course:any) => {this.courses = course.data;});
      this.types.get().subscribe((types:any) => {this.tipos = types;});
    this.form = formBuilder.group({
      archivo: ['',Validators.required],
      tipo: ['',Validators.required],
      base64: ['',Validators.required],
      first_name: ['', Validators.required],
      token: ['']
    });
    // Watch the form for changes, and
    this.form.valueChanges.subscribe((v) => {
      this.isReadyToSave = this.form.valid;
    });
  }

  getPicture() {
      this.fileInput.nativeElement.click();
  }

  processWebImage(event) {
    let reader = new FileReader();
    reader.onload = (readerEvent) => {

      let imageData = (readerEvent.target as any).result;
      let base64 = imageData.split(",");
      let data =base64[0].split(":");
      let base = data[1].split(";");
      let mime = base[0];
      this.form.patchValue({ 'tipo': mime });
      this.form.patchValue({ 'base64': base64[1] });
      this.form.patchValue({ 'token': sessionStorage.getItem("token") });
    };
    this.form.patchValue({ 'archivo': event.target.files[0].name });
    reader.readAsDataURL(event.target.files[0]);
    console.log(event.target.files[0]);
  }
  done() {
    if (!this.form.valid) { return 0; }
    console.log(this.form.value);
   // this.items.add(this.form.value);
    this.navCtrl.setRoot('ContentPage');
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad UploadFilesPage');
  }

}
