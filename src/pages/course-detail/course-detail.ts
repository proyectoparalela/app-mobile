import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { CourseService } from '../../providers';

@IonicPage()
@Component({
  selector: 'page-course-detail',
  templateUrl: 'course-detail.html'
})
export class CourseDetailPage {
  course: any;
  constructor(public navCtrl: NavController, navParams: NavParams, courses: CourseService) {
    this.course = navParams.get('course');
    // || courses.defaultItem;
  }

}
